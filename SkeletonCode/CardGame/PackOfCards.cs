﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonCode.CardGame
{
    public class PackOfCards : IPackOfCards
    {
        private Stack<ICard> pack = new Stack<ICard>();

        String[] values = { "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Ace", "Jack", "Queen", "King" };
        String[] suits = { "Hearts", "Diamonds", "Spades", "Clubs" };

        public int Count
        {
            get
            {
                return pack.Count();
            }
        }

        public IEnumerator<ICard> GetEnumerator()
        {
            return pack.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Create()
        {
            foreach(var suit in suits)
            {
                foreach(var val in values)
                {
                    pack.Push(new Card(val, suit));
                }
            }
        }

        public void Shuffle()
        {
            Random num = new Random();

            // initialize deck
            int n = suits.Length * values.Length;
            String[] pack = new String[n];

            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < suits.Length; j++)
                {
                    pack[suits.Length * i + j] = pack[i] + " of " + suits[j];
                }
            }

            // shuffle
            for (int i = 0; i < n; i++)
            {
                int r = i + (int)(num.NextDouble() * (n - i));
                String temp = pack[r];
                pack[r] = pack[i];
                pack[i] = temp;
            }
        }

        public ICard TakeCardFromTopOfPack()
        {
            ICard cardRemoved = pack.Pop();
            return cardRemoved;
        }
        
    }
}
