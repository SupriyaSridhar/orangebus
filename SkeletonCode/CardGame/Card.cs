﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonCode.CardGame
{
    public class Card : ICard
    {
        private String _value;
        private String _suit;

        public string Suit
        {
            get
            {
                return _suit;
            }

            set
            {
                _suit = value;
            }
        }

        public string Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
            }
        }

        public Card(String val, String suit)
        {
            Value = val;
            Suit = suit;
        }
       
        public override string ToString()
        {
            return Value + " of  " + Suit;
        }
    }
}
