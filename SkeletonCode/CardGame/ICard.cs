﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkeletonCode.CardGame
{
    public interface ICard
    {
        string Value { get; set; }

        string Suit { get; set; }

        // return represenation of card
        string ToString();

    }
}
