﻿using System;

namespace SkeletonCode.ReversingString
{
	public class StringUtilities
	{
        // String reversal without copy to char array
		public string Reverse(string input)
		{
            char[] inputstream = input.ToCharArray();
            Array.Reverse(inputstream);
            return new string(inputstream);
            
		}
	}
}
