﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonCode
{
    public interface ILog
    {
        void Log(TraceEventType e, string message);
    }
}
