﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Autofac;
using Microsoft.Extensions.Logging;
using Repository;
using Common;
using System.Collections.Generic;
using SkeletonCode.CurrencyConverter;

[assembly: OwinStartup(typeof(SkeletonCode.Startup))]

namespace SkeletonCode
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            var loggerFactory = new LoggerFactory();

            builder.RegisterInstance<ILog>(new LoggerAdapter(loggerFactory.CreateLogger("Currencies")));

            builder.RegisterType<CurrencyRepository>().AsImplementedInterfaces().WithProperty("Currencies", new Dictionary<String, Currency>()).InstancePerLifetimeScope();

            builder.RegisterType<ConverterService>().AsImplementedInterfaces().InstancePerLifetimeScope();

            var container = builder.Build();

            app.Use(container);
        }
    }
}
