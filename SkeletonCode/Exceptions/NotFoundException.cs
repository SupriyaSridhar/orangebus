﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonCode.CurrencyConverter.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(String message) : base(message)
        {
        }
    }
}
