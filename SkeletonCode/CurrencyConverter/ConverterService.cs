﻿using Common;
using SkeletonCode.CurrencyConverter.Exceptions;
using Repository;
using System.Diagnostics;

namespace SkeletonCode.CurrencyConverter
{
	public class ConverterService : IConverterService                                                                  
	{
        private ICurrencyRepository _repository;

        private ILog _logger;

        public ICurrencyRepository Repository
        {
            get
            {
                return _repository;
            }

            set
            {
                _repository = value;
            }
        }

        public ILog Logger
        {
            get
            {
                return _logger;
            }

            set
            {
                _logger = value;
            }
        }

        public ConverterService(ICurrencyRepository repository, ILog logger)
        {
            Repository = repository;
            Logger = logger;
        }


        public Currency Convert(string inputCurrency, string outputCurrency, decimal amount)
		{
            _logger.Log(TraceEventType.Information, "Calculating converted amount for the currency symbol : " + inputCurrency+ "to the currency symbol" +outputCurrency);

            decimal conversionFactor = 1;

            Currency inCurrency = null;
            Currency outCurrency = null;

            try
            {
                inCurrency = _repository.getCurrencyBySymbol(inputCurrency);
                outCurrency = _repository.getCurrencyBySymbol(outputCurrency);

                if (outputCurrency.Equals("USD"))
                {
                    conversionFactor = 1.25M;
                }

                else if (outputCurrency.Equals("GBP"))
                {
                    conversionFactor = 0.8M;
                }

                outCurrency.Amount = amount * conversionFactor;
            }

            catch(NotFoundException ex)
            {
                // Just logging for now, further exception handling to be done depending on where this is invoked
                // Ex: If used in a controller API, return NotFound()
                _logger.Log(TraceEventType.Error, ex.Message);
            }
            
            return outCurrency;
		}
        
    }
}