﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonCode.CurrencyConverter
{
    public interface IConverterService
    {
        Currency Convert(string inputCurrency, string outputCurrency, decimal amount);



    }
}
