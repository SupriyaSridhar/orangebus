﻿using NUnit.Framework;
using System.Dynamic;
using SkeletonCode.CurrencyConverter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using Common;
using Repository;
using SkeletonCode;
using Moq;
using Common.Exceptions;

namespace UnitTests.CurrencyConverter
{
	[TestClass]
	public class CurrencyConverterTests
	{
        private ConverterService converterService;

        [TestInitialize]
        public void Setup()
        {
            converterService = new ConverterService(new CurrencyRepository(), new Mock<ILog>().Object);
            Currency usCurrency = TestUtil.getDefaultUSCurrency();
            Currency gbpCurrency = TestUtil.getDefaultGBPCurrency();

            //Register sample set of currencies
            converterService.Repository.registerCurrency(usCurrency);
            converterService.Repository.registerCurrency(gbpCurrency);
        }

        [TestMethod]
		public void ItShouldConvertFromPoundsToDollarsCorrectly()
		{
			decimal amountInPounds = 1m;
            decimal expectedAmountInDollars = 1.25m;

            Currency result = converterService.Convert(CurrencyType.GBP.ToString(), CurrencyType.USD.ToString(), amountInPounds);
            NUnit.Framework.Assert.AreEqual(expectedAmountInDollars, result.Amount);
            NUnit.Framework.Assert.AreEqual(Constants.USCulture, result.CultureInfo);
        }

		[TestMethod]
		public void ItShouldConvertFromDollarsToPoundsCorrectly()
		{
			decimal amountInDollars = 1m;
			decimal expectedAmountInPounds = 0.8m;

            Currency result = converterService.Convert(CurrencyType.USD.ToString(), CurrencyType.GBP.ToString(), amountInDollars);

            NUnit.Framework.Assert.AreEqual(expectedAmountInPounds, result.Amount);
            NUnit.Framework.Assert.AreEqual(Constants.GBCulture, result.CultureInfo);
        }
		
		[TestMethod]
		[ExpectedException(typeof(System.Exception), AllowDerivedTypes = true)]
		public void AnExceptionShouldBeThrownIfAnUnknownCurrencyTypeIsPassedIn()
		{
            converterService.Convert(CurrencyType.USD.ToString(), "DDD", 100);
		}

        [TestMethod]
        public void testRegisterAndUnregisterCurrency()
        {
            Currency currency_out;
            currency_out = this.converterService.Repository.getCurrencyBySymbol(CurrencyType.USD.ToString());
            this.converterService.Repository.unregisterCurrency(CurrencyType.USD.ToString());
            this.converterService.Repository.unregisterCurrency(CurrencyType.GBP.ToString());
            NUnit.Framework.Assert.IsTrue(converterService.Repository.getCurrencies().Count == 0);
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidValueException))]
        public void testRegisterCurrencyDuplicated()
        {
            Currency currency = TestUtil.getDefaultUSCurrency();
            this.converterService.Repository.registerCurrency(currency);
            this.converterService.Repository.registerCurrency(currency);
        }
        
    }
}
