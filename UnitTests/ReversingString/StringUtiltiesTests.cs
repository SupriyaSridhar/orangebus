﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

using SkeletonCode.ReversingString;

namespace UnitTests.ReversingString
{
	[TestFixture]
	public class StringUtiltiesTests
	{
		[TestCase("", "")]
		[TestCase("skeleton", "noteleks")]
		[TestCase(null, "")]
        [ExpectedException(typeof(System.NullReferenceException), AllowDerivedTypes = true)]
        public void ReverseStringShouldReturnTheStringInTheReverseOrder(string input, string expectedResult)
		{
            string result = "";

            StringUtilities stringUtilities = new StringUtilities();
            
            if(input!=null)
			    result = stringUtilities.Reverse(input);

            NUnit.Framework.Assert.AreEqual(expectedResult, result);
		}
	}
}
