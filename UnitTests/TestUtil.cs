﻿using Common;
using System;
using System.Globalization;

namespace UnitTests
{
    public class TestUtil
    {
     
        public static Currency getDefaultUSCurrency()
        {
            Currency c = new Currency();
            CultureInfo USCulture = CultureInfo.CreateSpecificCulture("en-us");
            c.CurrencyTypeId = CurrencyType.USD;
            c.CultureInfo = USCulture;
            return c;
        }

        public static Currency getDefaultGBPCurrency()
        {
            Currency c = new Currency();
            CultureInfo GBPCulture = CultureInfo.CreateSpecificCulture("en-gb");
            c.CurrencyTypeId = CurrencyType.GBP;
            c.CultureInfo = GBPCulture;
            return c;
        }
    }
}
