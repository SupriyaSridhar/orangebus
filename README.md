## Assignment Solution - Orange Bus ##

Author: <mailto:supriya.s.86@gmail.com>



The solution is a basic demonstration of the problem solving strategy.

There are multiple ways to solve the problems, performance constraints have been considered for the implemented cases.

*Additional packages used*: 

* Autofac for dependency injection

* Microsoft.Extensions for logging

* Moq for unit testing

*Futher Enhancements*:

All data has been maintained in program memory and there is no usage of databases for now.

The Service class functionality can be exposed via a controller API endpoint (restful, data-oriented controller).

The API will be restful and consumable by client applicaions
