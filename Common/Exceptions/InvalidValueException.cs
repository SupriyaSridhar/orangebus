﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Exceptions
{
    // Exception throen when value is invalid
    public class InvalidValueException : Exception
    {
        public InvalidValueException(String message) : base(message)
        {
        }
    }
}
