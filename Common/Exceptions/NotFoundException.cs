﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Exceptions
{
    // Value not found in repository
    public class NotFoundException : Exception
    {
        public NotFoundException(String message) : base(message)
        {
        }
    }
}
