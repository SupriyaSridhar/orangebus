﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    // Currency types for conversion
    public enum CurrencyType
    {
        USD,
        GBP
    }
}
