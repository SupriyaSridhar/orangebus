﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class Constants
    {
        public static readonly CultureInfo USCulture = CultureInfo.CreateSpecificCulture("en-us");
        public static readonly CultureInfo GBCulture = CultureInfo.CreateSpecificCulture("en-gb");
    }
}
