﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Currency
    {
        private CultureInfo cultureInfo;

        private decimal amount;

        private CurrencyType currencyTypeId;

        public CultureInfo CultureInfo
        {
            get
            {
                return cultureInfo;
            }

            set
            {
                cultureInfo = value;
            }
        }

        public decimal Amount
        {
            get
            {
                return amount;
            }

            set
            {
                amount = value;
            }
        }

        public CurrencyType CurrencyTypeId
        {
            get
            {
                return currencyTypeId;
            }

            set
            {
                currencyTypeId = value;
            }
        }
    }
}
