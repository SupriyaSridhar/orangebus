﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface ICurrencyRepository
    {
        Currency getCurrencyBySymbol(string currencySymbol);

        //register a currency
        void registerCurrency(Currency currency);

        //unregister a currency
        void unregisterCurrency(String currencySymbol);

        //get list of registered currencies
        IDictionary<string, Currency> getCurrencies();
    }
}
