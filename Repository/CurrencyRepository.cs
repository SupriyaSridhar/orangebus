﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.Exceptions;

namespace Repository
{
    public class CurrencyRepository : ICurrencyRepository
    {
        //internal representation for currencies
        private IDictionary<string, Currency> currencies = null;

        public IDictionary<string, Currency> Currencies
        {
            get
            {
                return currencies;
            }

            set
            {
                currencies = value;
            }
        }

        public CurrencyRepository()
        {
            Currencies = new Dictionary<string, Currency>();
        }

        public IDictionary<string, Currency> getCurrencies()
        {
            return Currencies;
        }

        //Get the associated currency for the input symbol
        public Currency getCurrencyBySymbol(string currencySymbol)
        {
            Currency currency = null;
            bool isValueValid = false;

            if (currencySymbol != null)
                isValueValid = Currencies.TryGetValue(currencySymbol, out currency);

            if (!isValueValid)
            {
                throw new NotFoundException("Cannot find the currency " + currencySymbol + " in the list. Please register the currency first");
            }

            return currency;
        }

        /// <summary>
        /// register the incoming currency
        /// </summary>
        public void registerCurrency(Currency currency)
        {
            string errorMessage = "The currency with id " + currency.CurrencyTypeId.ToString() + "has already been registered";

            if (Currencies.ContainsKey(currency.CurrencyTypeId.ToString()))
            {
                throw new InvalidValueException(errorMessage);

            }

            Currencies.Add(currency.CurrencyTypeId.ToString(), currency);
        }

        /// <summary>
        /// unregister the stock with the specified symbol
        /// </summary>
        public void unregisterCurrency(string currencySymbol)
        {
            string errorMessage = "The currency with id " + currencySymbol + "has not been registered";

            if (!Currencies.ContainsKey(currencySymbol))
            {
                throw new NotFoundException(errorMessage);
            }

            Currencies.Remove(currencySymbol);
        }
    }
}
